var path = require('path');

const express = require('express')
const app = express()

var views_dir = path.join(__dirname, 'public', 'views');

app.use(express.static('public'))

//app.use('/static', express.static(path.join(__dirname, 'public')))

app.get('/', function(req, res) {
    res.sendFile(path.join(views_dir + '/table.html'));
});

app.listen(3200, () => console.log("Misaal D'souza on port 3200!"));

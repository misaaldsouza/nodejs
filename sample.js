var path = require('path')
var fs = require('fs');

var csv = require('fast-csv');

var ws = fs.createWriteStream(path.join(__dirname, 'my.csv'));

var header = [
    'name','surname','age','gender'
  ];

csv.
write([
  header,  
  [
    'John',
    'Snow',
    '26',
    'M'
  ], [
    'Clair',
    'White',
    '33',
    'F',
  ], [
    'Fancy',
    'Brown',
    '78',
    'F'
  ]
/*["a1","b1"],
["b2","c2"],
["c2","d2"]*/

	],{headers:true})
.pipe(ws);